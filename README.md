## Welcome!

GitLabAthens is a community of **people**

in Athens, Greece interested in

the GitLab platform

and open source project

|  |
| --:|
| https://meetup.com/gitlabathens |
| https://twitter.com/gitlabathens |
| https://discord.gg/ym3ZU9Y |
| https://gitlab.com/athensmeetup |

---

## Don't forget to have fun...

Once there is no COVID-19 we'll go for beers!

You don't talk to professors or machines so express yourself. Feel free to:

1. share your id at gitlab/discord/twitter so that people can get in touch with you
2. open your camera
3. use emojis
4. say thanks
5. whatever helps in adding emotions in a constructive discussion...

Note: Press f for fullscreen and s to have a separate window with a timer and notes. COVID-19 looks like a jira ticket, it will be resolved but it takes a hell of a time

---

## Code of conduct

If you notice unacceptable behavior,

contact us and we might:

1. warn
2. suspend account temporarily
3. expel
4. refuse admittance

Note: We want a safe place and there is no point in talking about a code of conduct if there are no consequences

---

## Gitlab values C.R.E.D.I.T.

:handshake: = Collaboration

:chart_with_upwards_trend: = Results

:stopwatch: = Efficiency

:globe_with_meridians: = Diversity

:footprints: = Iteration

:eye: = Transparency

![emojis](https://about.gitlab.com/images/handbook/values-emoji.png)

Note: For example, you can use the following emojis to celebrate such cases.

---

## B instead of A

A: You could Google this in 5 seconds.

B: This is called Invariance and Covariance. If you Google it, you’ll find tutorials that can explain it much better than we can in an answer here.

Note: Examples from https://stackoverflow.com/conduct

---

## B instead of A

A: If you bothered to read my question, you’d know it’s not a duplicate.

B: I don’t think this is a duplicate. My question is about cement board, while the question you linked is about drywall.

---

## B instead of A

A: Are you speaking English? If so, I can’t tell.

B: I’m having trouble understanding your question. I think you’re asking how to add a swap after system installation. Is that correct?

---

## B instead of A

A: I came to get help, not to get my question edited.

B: Thanks for trying to help, but your edit isn’t what I meant. I’ve removed your edit, and have updated my question so it’s clearer.

---

## To give help

1. be patient, welcoming and respectful

2. be inclusive

2. when giving feedback, be clear and constructive

---

## To get help

Same as when giving help plus:

1. make it as easy as possible for others to help you

2. be open when receiving feedback

---

## Our values

1. Code of Conduct
2. Low level of shame
3. Bias for Action
4. Respect people's time and attention
5. add more...

---

## At gitlab.com

Create an issue for everything you would like to ask/do.

To state a topic you would like to watch and gather interest, you can open an issue.

To present one, choose the template topic-to-present.

e.g. today's topic: https://gitlab.com/athensmeetup/events/-/issues/15

---

## We value feedback

Don't forget to leave your feedback

and help improve such events!

https://forms.gle/sFt6UqUXCcEtys9n6
